\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Convex Optimization --- DM 1
	\noindent\rule{\linewidth}{1pt}
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\trans}{\mathsf{T}}
\newcommand{\dist}{\text{\textbf{dist}}}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{Exercise 2.12}
	
	\paragraph{(a)}
	For $x \in \R^n$ we have $\alpha \leqslant a^\trans x \leqslant \beta \Leftrightarrow \left\{ \begin{array}{lll}
	- a^\trans x & \leqslant & - \alpha \\
	a^\trans x & \leqslant & \beta
	\end{array} \right.$. \\
	Then a slab:
	$$ \left\{ x \in \R^n ~|~ \alpha \leqslant a^\trans x \leqslant \beta \right\} = \left\{ x \in \R^n ~|~ - a^\trans x \leqslant - \alpha \right\} \cap \left\{ x \in \R^n ~|~ a^\trans x \leqslant - \beta \right\} $$
	is the intersection of two halfspaces which are convex sets. Then \fbox{a slab is convex}.
	
	\paragraph{(b)}
	We denote by $e_i$ the vector with a one at position $i$ and zeros everywhere else. We then have :
	$$ \left\{ x \in \R^n ~|~ \alpha_i \leqslant x_i \leqslant \beta_i, \; i = 1, ..., n \right\} = \bigcap_{i = 1}^n \left\{ x \in \R^n ~|~ \alpha_i \leqslant e_i^\trans x \leqslant \beta_i \right\} $$
	Then a rectangle is the intersection of $n$ slabs. Because slabs are convex, \fbox{a rectangle is convex}.
	
	\paragraph{(c)}
	A wedge can be written as the intersection of two halfspaces :
	$$ \left\{ x \in \R^n ~|~ a_1^\trans x \leqslant b_1, \; a_2^\trans x \leqslant b_2 \right\} = \left\{ x \in \R^n ~|~ a_1^\trans x \leqslant b_1 \right\} \cap \left\{ x \in \R^n ~|~ a_2^\trans x \leqslant b_2 \right\} $$
	Because halfspaces are convex, \fbox{a wedge is convex}.
	
	\paragraph{(d)}
	We rewrite our set as an intersection of many sets:
	$$ T = \left\{ x ~|~ \| x - x_0 \|_2 \leqslant \| x - y \|_2 \text{ for all } y \in S \right\} = \bigcap_{y \in S} \left\{ x ~|~ \| x - x_0 \|_2 \leqslant \| x - y \|_2 \right\} $$
	Then we rewrite the condition on $x$ :
	$$ \begin{array}{lll}
	\| x - x_0 \|_2 \leqslant \| x - y \|_2 & \Leftrightarrow & \| x - x_0 \|_2^2 \leqslant \| x - y \|_2^2 \\[2mm]
	& \Leftrightarrow & x^\trans x - 2 x^\trans x_0 + x_0^\trans x_0 \leqslant x^\trans x - 2 x^\trans y + y^\trans y \\[1mm]
	& \Leftrightarrow & 2 x^\trans (y - x_0) \leqslant y^\trans y - x_0^\trans x_0 \\[2mm]
	& \Leftrightarrow & (y - x_0)^\trans x \leqslant \frac{1}{2} \left( \| y \|_2^2 - \| x_0 \|_2^2 \right)
	\end{array} $$
	We then have :
	$$ T = \bigcap_{y \in S} \left\{ x ~|~ (y - x_0)^\trans x \leqslant \frac{1}{2} \left( \| y \|_2^2 - \| x_0 \|_2^2 \right) \right\} $$
	So $T$ is the intersection of many halfspaces which are convex sets. Then \fbox{$T$ is convex}.
	
	\paragraph{(e)}
	We consider the set:
	$$ U = \left\{ x ~|~ \dist(x, S) \leqslant \dist(x, T) \right\} $$
	We will show that $U$ is not necessarily convex by taking the example with $n = 1$, $S = \{-1, 1\}$ and $T = \R \setminus S$. \\
	For $x \in \R$. Because $T$ is dense in $\R$ we have $\dist(x, T) = 0$. Then $x$ is in $U$ if and only if $\dist(x, S)~=~0 \Leftrightarrow x \in S$. Thus $U = S$. $S$ is clearly not convex. So \fbox{$U$ is not convex}.
	
	\paragraph{(f)}
	We rewrite our set as an intersection of many sets:
	$$ T = \left\{ x ~|~ x + S_2 \subseteq S_1 \right\} = \bigcap_{y \in S_2} \left\{ x ~|~ x + y \in S_1 \right\} = \bigcap_{y \in S_2} \left\{ z - y ~|~ z \in S_1 \right\} = \bigcap_{y \in S_2} f_y(S_1) $$
	With $S_1 \subseteq \R^n$ a convex set and $f_y(z) = z - y$ an affine function. We know that the image of a convex set under an affine function is convex. So $f_y(S_1)$ is a convex set for all $y \in S_2$. Thus $T$ is the intersection of many convex sets. So \fbox{$T$ is convex}.
	
	\paragraph{(e)}
	We rewrite the condition on $x$:
	$$ \begin{array}{lll}
	\| x - a \|_2 \leqslant \theta \| x - b \|_2 & \Leftrightarrow & \| x - a \|_2^2 \leqslant \theta^2 \| x - b \|_2^2 \\[2mm]
	& \Leftrightarrow & x^\trans x - 2 x^\trans a + a^\trans a \leqslant \theta^2 x^\trans x - 2 \theta^2 x^\trans b + \theta^2 b^\trans b \\[1mm]
	& \Leftrightarrow & (1 - \theta^2) x^\trans x + 2 x^\trans (\theta^2 b - a) \leqslant \theta^2 b^\trans b - a^\trans a \\[2mm]
	& \Leftrightarrow & (1 - \theta^2) \| x \|_2^2 + ( \theta^2 b - a)^\trans x \leqslant \frac{1}{2} \left( \| \theta b \|_2^2 - \| a \|_2^2 \right)
	\end{array} $$
	We then consider the function $f(x) = (1 - \theta^2) \| x \|_2^2 + ( \theta^2 b - a)^\trans x$. It is supposed that $\theta < 1$ then $(1 - \theta^2) > 0$. Thus $f$ is the sum of an affine function and a squared norm multiplied by a positive scalar. Because $f$ is a sum of convex function, $f$ is convex. We consider $\alpha = \frac{1}{2} \left( \| \theta b \|_2^2 - \| a \|_2^2 \right)$. Then the set:
	$$ S = \left\{ x ~|~ \| x - a \|_2 \leqslant \theta \| x - b \|_2 \right\} = \left\{ x ~|~ f(x) \leqslant \alpha \right\} $$
	is the $\alpha$-sublevel set of $f$. So \fbox{$S$ is convex}.
	
	\section*{Exercise 3.21}
	
	\paragraph{(a)}
	We know that all norms on $\R^n$ are convex. Furthermore we also know that composition with affine function preserve convexity. So the function:
	$$ f_i(x) = \left\| A^{(i)} x - b^{(i)} \right\| $$
	is convex. Finally we know that pointwise maximum of convex functions is convex. So:
	\begin{center}
		\fbox{$ \displaystyle f : x \mapsto \max_{i = 1, ..., k} f_i(x) \; $ is convex}
	\end{center}

	\paragraph{(b)}
	We denote by $R$ a subset of $\{1, ..., n\}$ of size $r$. Then we denote by $f_R$ the function:
	$$ f_R(x) = \sum_{i \in R} |x_i| $$
	If we denote by $e_R$ the vector which have ones at every positions in $R$ and zeros everywhere else, we obtain the following equality : $f_R(x) = \| e_R^\trans x \|_1$. We know that norms are convex and that composition with affine functions preserve convexity. So $f_R$ is convex. \\
	For $S$ the set of indices of the $r$ largest components of $|x|$ we have $f_S(x) = f(x)$. Furthermore for every $R$ subset of $\{1, ..., n\}$ of size $r$ we have $f_R(x) \leqslant f(x)$. So:
	$$ f(x) = \max_{R \subseteq \{1, ..., n\} \atop |R| = r} f_R(x) $$
	Then $f$ is a pointwise maximum of convex functions. So \fbox{$f$ is convex}.
	
	\section*{Exercise 3.32}
	
	\paragraph{(a)}
	Let $x < y$ and $0 \leqslant \theta \leqslant 1$. Then we let $z = \theta x + (1 - \theta) y$. \\
	To show that $fg$ is convex, we need to show that:
	$$ f(z)g(z) \leqslant \theta f(x)g(x) + (1 - \theta) f(y)g(y) $$
	Because $g$ is positive and $f$ convex, we have :
	$$ f(z) g(z) \leqslant \left[ \theta f(x) + (1 - \theta) f(y) \right] g(z) $$
	Because $f$ is positive and $g$ convex, we now have :
	\begin{equation}
	\begin{array}{lll}
	f(z) g(z) & \leqslant & \left[ \theta f(x) + (1 - \theta) f(y) \right] \left[ \theta g(x) + (1 - \theta) g(y) \right] \\
	& \leqslant & \theta^2 f(x)g(x) + \theta (1 - \theta) \left[ f(x)g(y) + f(y)g(x) \right] + (1 - \theta)^2 f(y)g(y)
	\end{array} 
	\label{beg}
	\end{equation}
	Then we remarks :
	\begin{equation}
		\left( f(x) - f(y) \right) \left( g(y) - g(x) \right) \leqslant 0 \Leftrightarrow f(x)g(y) + f(y)g(x) \leqslant f(x)g(x) + f(y)g(y)
		\label{ineq}
	\end{equation}
	We now consider the two cases :
	\begin{itemize}
		\item If $f$ and $g$ are non-decreasing. Then $f(x) - f(y) \leqslant 0$ and $g(y) - g(x) \geqslant 0$ so from the equivalence \eqref{ineq}, we have :
		$$ f(x)g(y) + f(y)g(x) \leqslant f(x)g(x) + f(y)g(y) $$
		\item If $f$ and $g$ are non-increasing. Then $f(x) - f(y) \geqslant 0$ and $g(y) - g(x) \leqslant 0$ so from the equivalence \eqref{ineq}, we have :
		$$ f(x)g(y) + f(y)g(x) \leqslant f(x)g(x) + f(y)g(y) $$
	\end{itemize}
	In both cases we obtain the same conclusion that we can use in \eqref{beg} to obtain:
	$$ \begin{array}{lll}
	f(z) g(z) & \leqslant & \theta^2 f(x)g(x) + \theta (1 - \theta) \left[ f(x)g(x) + f(y)g(y) \right] + (1 - \theta)^2 f(y)g(y) \\
	& \leqslant & \left[ \theta^2 + \theta (1 - \theta) \right] f(x)g(x) + \left[ (1 - \theta)^2 + \theta (1 - \theta) \right] f(y)g(y) \\
	& \leqslant & \theta f(x)g(x) + (1 - \theta) f(y)g(y)
	\end{array} $$
	So we have that \fbox{$fg$ is convex}.
	
	\paragraph{(b)}
	Let $x < y$ and $0 \leqslant \theta \leqslant 1$. Then we let $z = \theta x + (1 - \theta) y$. \\
	To show that $fg$ is convex, we need to show that:
	$$ f(z)g(z) \geqslant \theta f(x)g(x) + (1 - \theta) f(y)g(y) $$
	Because $g$ is positive and $f$ concave, we have :
	$$ f(z) g(z) \geqslant \left[ \theta f(x) + (1 - \theta) f(y) \right] g(z) $$
	Because $f$ is positive and $g$ concave, we now have :
	\begin{equation}
	\begin{array}{lll}
	f(z) g(z) & \geqslant & \left[ \theta f(x) + (1 - \theta) f(y) \right] \left[ \theta g(x) + (1 - \theta) g(y) \right] \\
	& \geqslant & \theta^2 f(x)g(x) + \theta (1 - \theta) \left[ f(x)g(y) + f(y)g(x) \right] + (1 - \theta)^2 f(y)g(y)
	\end{array} 
	\label{beg2}
	\end{equation}
	Then we remarks :
	\begin{equation}
	\left( f(x) - f(y) \right) \left( g(y) - g(x) \right) \geqslant 0 \Leftrightarrow f(x)g(y) + f(y)g(x) \geqslant f(x)g(x) + f(y)g(y)
	\label{ineq2}
	\end{equation}
	We now consider the two cases :
	\begin{itemize}
		\item If $f$ is non-increasing and $g$ is non-decreasing. Then $f(x) - f(y) \geqslant 0$ and $g(y) - g(x) \geqslant 0$ so from the equivalence \eqref{ineq2}, we have :
		$$ f(x)g(y) + f(y)g(x) \geqslant f(x)g(x) + f(y)g(y) $$
		\item If $f$ is non-decreasing and $g$ is non-increasing. Then $f(x) - f(y) \leqslant 0$ and $g(y) - g(x) \leqslant 0$ so from the equivalence \eqref{ineq2}, we have :
		$$ f(x)g(y) + f(y)g(x) \geqslant f(x)g(x) + f(y)g(y) $$
	\end{itemize}
	In both cases we obtain the same conclusion that we can use in \eqref{beg2} to obtain:
	$$ \begin{array}{lll}
	f(z) g(z) & \geqslant & \theta^2 f(x)g(x) + \theta (1 - \theta) \left[ f(x)g(x) + f(y)g(y) \right] + (1 - \theta)^2 f(y)g(y) \\
	& \geqslant & \left[ \theta^2 + \theta (1 - \theta) \right] f(x)g(x) + \left[ (1 - \theta)^2 + \theta (1 - \theta) \right] f(y)g(y) \\
	& \geqslant & \theta f(x)g(x) + (1 - \theta) f(y)g(y)
	\end{array} $$
	So we have that \fbox{$fg$ is concave}.
	
	\paragraph{(c)}
	We start from the inequality :
	$$ 0 \leqslant \left( g(x) - g(y) \right)^2 = g(x)^2 + g(y)^2 - 2 g(x) g(y) $$
	Let $0 \leqslant \theta \leqslant 1$. We then multiply by $\theta (1 - \theta) \geqslant 0$ :
	$$ 0 \leqslant \theta (1 - \theta) \left( g(x)^2 + g(y)^2 - 2 g(x) g(y) \right) = \theta (1 - \theta) \left( g(x)^2 + g(y)^2 \right) + \left( \theta^2 + (1 - \theta)^2 - 1 \right) g(x) g(y) $$
	We add $g(x)g(y)$ to this inequality :
	$$ \begin{array}{lll}
	g(x) g(y) & \leqslant & \theta (1 - \theta) \left( g(x)^2 + g(y)^2 \right) + \left( \theta^2 + (1 - \theta)^2 \right) g(x) g(y) \\
	& \leqslant & \left[ \theta g(x) + (1 - \theta) g(y) \right] \left[ \theta g(y) + (1 - \theta) g(x) \right]
	\end{array} $$
	Because $g$ is positive we can divide by $g(x)g(y) \left[ \theta g(x) + (1 - \theta) g(y) \right]$. This gives the following interesting result :
	\begin{equation}
		\dfrac{1}{\theta g(x) + (1 - \theta) g(y)} \leqslant \dfrac{\theta}{g(x)} + \dfrac{1 - \theta}{g(y)}
		\label{inv}
	\end{equation}
	Now, let $z = \theta x + (1 - \theta) y$. Because $g$ is concave, we get :
	$$ g(z) \geqslant \theta g(x) + (1 - \theta) g(y) $$
	Because $g$ is positive the two sides of the inequality are non zero. We can take the inverse of each sides and inverse the inequality to obtain :
	$$ \dfrac{1}{g(z)} \leqslant \dfrac{1}{g(x) + (1 - \theta) g(y)} $$
	By using the inequality \eqref{inv} we finally get :
	$$ \dfrac{1}{g(z)} \leqslant \dfrac{\theta}{g(x)} + \dfrac{1 - \theta}{g(y)} $$
	Then the function $1/g$ is convex, positive and non-decreasing. Indeed because $g$ is non-increasing, its inverse is non-decreasing. Then $f$ and $1/g$ satisfy the conditions of the question \textbf{(a)}. So their product is convex. This means that \fbox{$f/g$ is convex}.

	\section*{Exercise 3.36}
	
	\paragraph{(a)}
	We have :
	$$ f^*(y) = \max_{x \in \R^n} g(x, y) \qquad \text{where } g(x, y) = y^\trans x - \max_{i = 1, ..., n} x_i $$
	In the case $n = 1$ we have $g(x, y) = (y - 1) x$. Then :
	$$ n = 1 \quad \Rightarrow \quad f^*(y) = \left\{ \begin{array}{lrr}
		0 & \text{if} & y = 1 \\
		+ \infty & \text{if} & y \neq 1
	\end{array} \right. \vspace{5mm} $$
	Now we consider $n > 1$. If there exists $i \in \{1, ..., n\}$ such that $y_i < 0$ then we let $\lambda > 0$. Because $n > 1$, we have $\max_j (-\lambda e_i)_j = 0$. Therefore we obtain :
	$$ g(- \lambda e_i, y) = - \lambda y_i \xrightarrow[\lambda \rightarrow + \infty]{} +\infty $$
	Where $e_i$ is the vector with a one at position $i$ and zeros everywhere else. Then if $y$ has a negative component $f^*(y) = +\infty$. \\
	We now look at the case $y \in \R_+^n$. We denote by $\mathbbm{1}_n$ the vector with ones everywhere. We also denote by $m(x)$, the maximum $\max_{i = 1, ..., n} x_i$. Then $m \left( m(x) \mathbbm{1}_n \right) = m(x)$ and by positivity of $y$ we have $y^\trans x \leqslant y^\trans \left( m(x) \mathbbm{1}_n \right)$. Thus :
	$$ g(x, y) \leqslant g \left( m(x) \mathbbm{1}_n, y \right) $$
	This result allow us to reduce our maximum on $\R_n$ to a maximum on $\R$ :
	$$ f^*(y) = \max_{x \in \R} \; g(x \mathbbm{1}_n, y) = \max_{x \in \R} \; x \left( \| y \|_1 - 1 \right) $$
	We then obtain :
	\begin{center}
		\fbox{$\displaystyle f^*(y) = \left\{ \begin{array}{ll}
			0 & \text{if } \; y \in \R_+^n, \; \| y \|_1 = 1 \\
			+ \infty & \text{otherwise}
			\end{array} \right.$}
	\end{center}
	This is for the case $n > 1$. But we can remark that for $n = 1$ the formula is still true. So this is a correct expression in the general case.

	\paragraph{(b)}
	This time we have :
	$$ g(x, y) = y^\trans x - \sum_{i = 1}^r x_{[i]} $$
	We start with the case $r = n$. In this case we have $g(x, y) = (y - \mathbbm{1}_n)^\trans x$. So:
	$$ r = n \quad \Rightarrow \quad f^*(y) = \left\{ \begin{array}{lrr}
			0 & \text{if} & y = \mathbbm{1}_n \\
			+ \infty & \text{if} & y \neq \mathbbm{1}_n
			\end{array} \right. \vspace{5mm} $$
	Now we consider the case $r < n$. As in the previous question if there exists $i$ such that $y_i < 0$, then for $\lambda > 0$, because $r < n$, we have $\sum_{j = 1}^r (-\lambda e_i)_{[j]} = 0$ and :
	$$ g(- \lambda e_i, y) = - \lambda y_i \xrightarrow[\lambda \rightarrow + \infty]{} +\infty $$
	Otherwise $y \succeq 0$. Suppose now that there exists $i$ such that $y_i > 1$. Then for $\lambda > 0$, we have:
	$$ g(\lambda e_i, y) = \lambda (y_i - 1) \xrightarrow[\lambda \rightarrow +\infty]{} +\infty $$
	We are now restricted to the case $0 \preceq y \preceq \mathbbm{1}_n$. Suppose $y^\trans \mathbbm{1}_n < r$ that is to say $\| y \|_1 < r$. Then :
	$$ g(y, \lambda \mathbbm{1}_n) = \lambda (\| y \|_1 - r) \xrightarrow[\lambda \rightarrow - \infty]{} +\infty $$
	In the same way if $\| y \|_1 > r$, we have :
	$$ g(y, \lambda \mathbbm{1}_n) = \lambda (\| y \|_1 - r) \xrightarrow[\lambda \rightarrow + \infty]{} +\infty $$
	We finally study the case where $0 \preceq y \preceq \mathbbm{1}_n$ and $\| y \|_1 = r$. We fix $x \in \R^n$. We consider the Linear Knapsack Problem (LKP) where $(x_i - x_{[n]})$'s are the (non-negative) values of $n$ objects. Where we can pick at most a quantity $1$ of each objects and where the size of our knapsack is $r$. We know that 'the' greedy algorithm is optimal for (LKP). So picking a quantity $1$ of the $r$ objects with the highest values is optimal. This means that for $y$ with $0 \preceq y \preceq \mathbbm{1}_n$ and $\| y \|_1 = r$, a vector representing the amount of each objects that we pick, we have :
	$$ y^\trans (x - x_{[n]} \mathbbm{1}_n) \leqslant \sum_{i = 1}^r (x_{[i]} - x_{[n]}) $$
	Because $ y^\trans \mathbbm{1}_n = \| y \|_1 = r$, we can add $x_{[n]} y^\trans \mathbbm{1}_n = r x_{[n]}$ to both sides to obtain :
	$$ y^\trans x \leqslant \sum_{i = 1}^r x_{[i]} $$
	So $g(x, y) \leqslant 0$ and $g(0, y) = 0$. This gives $f^*(y) = 0$ in this case. \\[2mm]
	Finally we regroup all cases:
	\begin{center}
		\fbox{$\displaystyle f^*(y) = \left\{ \begin{array}{ll}
			0 & \text{if } \; 0 \preceq y \preceq \mathbbm{1}_n, \; \| y \|_1 = r \\
			+ \infty & \text{otherwise}
			\end{array} \right.$}
	\end{center}
	This is for the case $r < n$. But we can remark that for $r = n$ the formula is still true. So this is a correct expression in the general case.
	
	
	\paragraph{(c)}
	$$ f^*(y) = \max_{x \in \R} g(x, y) \qquad \text{where } g(x, y) = yx - max_{i = 1, ..., m} \; (a_ix + b_i) $$
	For $i < j$, we know that the equation $a_i x + b_i = a_j x + b_j$ admits a solution because the affine functions are not redundant. We write $x_{i, j}$ this solution :
	$$ x_{i, j} = \dfrac{b_j - b_i}{a_i - a_j} \qquad \text{and } \left\{ \begin{array}{lll}
	x > x_{i, j} & \Rightarrow & a_i x + b_i < a_j x + b_j \\
	x < x_{i, j} & \Rightarrow & a_i x + b_i > a_j x + b_j
	\end{array} \right.$$
	For $x < x_{1, 2}$, we have $f(x) > a_2 x + b_2$. If there exists $i > 2$ such that $f(x) = a_i x + b_i$ then $x_{2, i} \leqslant x < x_{1, 2}$. So for $x' \geqslant x_{1, 2}$, we have $x' > x_{2, i}$ then $f(x) \geqslant a_i x + b_i > a_2 x + b_2$. This means that $a_2 x + b_2$ is redundant. This is a contradiction. So for $x < x_{1, 2}$, we have $f(x) = a_1 x + b_1$. Furthermore, we also have that for all $i > 2$, $x_{1, 2} \leqslant x_{2, i}$. \\
	Now consider $x_{1, 2} < x < x_{2, 3}$. We know that $f(x) > a_1 x + b_1$ and $f(x) > a_3 x + b_3$. If there exists $i > 3$ such that $f(x) = a_i x + b_i$ then $x_{3, i} \leqslant x < x_{2, 3}$. So for $x' \geqslant x_{2, 3}$, we have $x' > x_{3, i}$ then $f(x) \geqslant a_i x + b_i > a_3 x + b_3$. This means that $a_3 x + b_3$ is redundant. This is a contradiction. So for $x_{1, 2} < x < x_{2, 3}$, we have $f(x) = a_2 x + b_2$. Furthermore, we also have that for all $i > 3$, $x_{2, 3} \leqslant x_{3, i}$. \\
	By recurrence we end up obtaining :
	$$ x_{1, 2} \leqslant x_{2, 3} \leqslant ... \leqslant x_{m-1, m} \qquad
	f(x) = \left\{ \begin{array}{llc}
	a_1 x + b_1 & \text{if} & x < x_{1, 2} \\
	a_i x + b_i & \text{if} & x_{i-1, i} \leqslant x < x_{i, i+1} \\
	a_m x + b_m & \text{if} & x_{m-1, m} \leqslant x
	\end{array} \right. $$
	Then for $y < a_1$ and $x < x_{1, 2}$, we have:
	$$ g(x, y) = x (y - a_1) - b_1 \xrightarrow[x \rightarrow -\infty]{} + \infty $$
	For $y > a_m$ and $x > x_{m-1, m}$, we have:
	$$ g(x, y) = x (y - a_m) - b_m \xrightarrow[x \rightarrow +\infty]{} + \infty $$
	Finally for $a_i \leqslant y \leqslant a_{i+1}$ we compute the derivative of $g$ with respect to $x$:
	$$ \dfrac{\partial g}{\partial x}(x, y) = \left\{ \begin{array}{llc}
	y - a_1 & \text{if} & x < x_{1, 2} \\
	y - a_j & \text{if} & x_{j-1, j} \leqslant x < x_{j, j+1} \\
	y - a_m & \text{if} & x_{m-1, m} \leqslant x
	\end{array} \right. $$
	Then $g(., y)$ is non-decreasing for $x \leqslant x_{i, i+1}$ and non-increasing for $x \geqslant x_{i, i+1}$. Then:
	$$ f^*(y) = g(x_{i, i+1}, y) = (y - a_i) \dfrac{b_j - b_i}{a_i - a_j} - b_i $$
	We can now write:
	\begin{center}
		\fbox{$\displaystyle f^*(y) = \left\{ \begin{array}{llc}
			+\infty & \text{if} & y < a_1 \text{ or } a_m < y \\
			(y - a_i) \dfrac{b_j - b_i}{a_i - a_j} - b_i & \text{if} & a_i \leqslant y \leqslant a_{i+1}
			\end{array} \right.$}
	\end{center}
	
	\paragraph{(d)}
	$$ f^*(y) = \max_{x \in \R_{++}} g(x, y) \qquad \text{where } g(x, y) = yx - x^p $$
	We start with the case $p > 1$. If $y \leqslant 0$, then $g(x, y) \leqslant 0$ because $x$ is positive. Furthermore when $x$ tends to 0, $g(x, y)$ tends also to 0. So in this case $f^*(y) = 0$. \\[2mm]
	Now consider $y > 0$. On $\R_{++}$, $x \mapsto x^p$ is strictly convex so $g(x, y)$ is strictly concave with respect to $x$. Then $g(., y)$ admits a unique maxima on $\R_{++}$. We derive $g$ with respect to $x$:
	$$ \dfrac{\partial g}{\partial x} = y - p x^{p-1} $$
	The maximum is reached when this derivative equals 0. So when $x = \left( \frac{y}{p} \right)^{\frac{1}{p-1}}$.
	$$ f^*(y) \; = \; g \left( \left( \frac{y}{p} \right)^{\frac{1}{p-1}}, \, y \right) \; = \; y^{\frac{p}{p-1}} p^{-\frac{1}{p-1}} - y^{\frac{p}{p-1}} p^{-\frac{p}{p-1}} \; = \; (p - 1) \left( \frac{y}{p} \right)^{\frac{p}{p-1}} $$
	We then obtain:
	\begin{center}
		\fbox{$\displaystyle p > 1 \quad \Rightarrow \quad f^*(y) = \left\{ \begin{array}{ll}
			0 & \text{If } y \leqslant 0 \\
			(p - 1) \left( \frac{y}{p} \right)^{\frac{p}{p-1}} & \text{If } y > 0
			\end{array} \right.$}
	\end{center}
	\vspace{5mm}
	
	Now we study the case $p < 0$. If $y > 0$ then $g(x, y)$ tends to infinity when $x$ tends to infinity. So in this case $f^*(y) = +\infty$. \\[2mm]
	Then we look at the case $y \leqslant 0$. We still have the strict concavity of $g(., y)$. The derivative is still the same and the maxima has still the same expression. So we obtain the final result :
	\begin{center}
		\fbox{$\displaystyle p < 0 \quad \Rightarrow \quad f^*(y) = \left\{ \begin{array}{ll}
			+ \infty & \text{If } y > 0 \\
			(p - 1) \left( \frac{y}{p} \right)^{\frac{p}{p-1}} & \text{If } y \leqslant 0
			\end{array} \right.$}
	\end{center}
	
	\paragraph{(e)}
	$$ f^*(y) = \max_{x \in \R_{++}^n} g(x, y) \qquad \text{where } g(x, y) = y^\trans x + \left( \prod_{i=1}^n x_i \right)^{1/n} $$
	First we can remark that when there exists $i$ such that $y_i > 0$ then :
	$$ g(\lambda e_i, y) \geqslant \lambda y_i \xrightarrow[\lambda \rightarrow +\infty]{} +\infty $$
	So in this case, $f^*(y) = +\infty$. \\[2mm]
	In the same way, if there exists $i$ such that $y_i = 0$ then for $\lambda > 0$ we have:
	$$ g(\lambda e_i + \mathbbm{1}_n, y) = y^\trans \mathbbm{1}_n + (\lambda + 1) \xrightarrow[\lambda \rightarrow +\infty]{} +\infty $$
	So we have again, $f^*(y) = +\infty$. \\[2mm]
	Now consider the case $y \prec 0$. We use the inequality of arithmetic and geometric means on the components of the vector $(-y_i x_i)_{1 \leqslant i \leqslant n}$, which is a vector with non-negative components:
	$$ - \dfrac{y^\trans x}{n} \geqslant \left( \prod_{i=1}^n (-y_i x_i) \right)^{\frac{1}{n}} = \left( \prod_{i=1}^n -y_i \right)^{\frac{1}{n}} \left( \prod_{i=1}^n x_i \right)^{\frac{1}{n}} $$
	We then obtain:
	\begin{equation}
		\left( 1 - n \left( \prod_{i=1}^n -y_i \right)^{\frac{1}{n}} \right) \left( \prod_{i=1}^n x_i \right)^{\frac{1}{n}} \geqslant g(x, y)
		\label{geom}
	\end{equation}
	Furthermore there is equality when all $-y_i x_i$ are equal so when $x_i = -\lambda/y_i$ for $\lambda \geqslant 0$. We denote by $z$ the vector such that $z_i = -1/y_i$, and we denote by $Y$ the value $\left( \prod_{i=1}^n -y_i \right)^{\frac{1}{n}}$. We then have:
	$$ (1 - nY) \lambda / Y = g(\lambda z, y) $$
	If $nY < 1$ then:
	$$ g(\lambda z, y) \xrightarrow[\lambda \rightarrow +\infty]{} +\infty $$
	Otherwise if $nY \geqslant 1$ then $g(., y)$ is non-positive thanks to the inequality \eqref{geom}. Because $g(0, y) = 0$ we then have $f^*(y) = 0$ in this case. \\
	Finally we obtain:
	\begin{center}
		\fbox{$\displaystyle f^*(y) = \left\{ \begin{array}{ll}
			0 & \text{If } y \prec 0 \text{ and } n \left( \prod_{i=1}^n -y_i \right)^{\frac{1}{n}} \geqslant 0 \\
			+ \infty & \text{Otherwise}
			\end{array} \right.$}
	\end{center}
	
	
	\paragraph{(f)}
	$$ f^*(y, u) = \max_{(x, t) \in K} g(x, t, y, u) $$
	$$ \text{where } g(x, y) = y^\trans x + tu + \log \left( t^2 - x^\trans x \right) \quad \text{ and } \quad K = \{ (x, t) \in \R^n \times \R ~|~ \| x \|_2 < t \} $$
	First we suppose that $\| y \|_2 \geqslant -u$. We now pose $x = \lambda y$ and $t = \lambda \| y \|_2 + 1$ with $\lambda > 0$. We then have:
	$$ g(x, t, y, u) = \lambda \| y \|_2^2 + (\lambda \| y \|_2 + 1) u + \log \left( (\lambda \| y \|_2 + 1)^2 - \lambda^2 \| y \|_2^2 \right) $$
	Now we use the fact that $u \geqslant - \| y \|_2$:
	$$ g(x, t, y, u) = - \| y \|_2 + \log \left( 2 \lambda \| y \|_2 + 1 \right) \xrightarrow[\lambda \rightarrow +\infty]{} +\infty $$
	So when $\| y \|_2 \geqslant -u$, we have $f^*(y, u) = +\infty$. \\[2mm]
	Now we suppose $\| y \|_2 < -u$. The function $(x, t) \mapsto y^\trans x + tu$ is linear then it is concave. Now we will show that $h : (x, t) \mapsto log \left( t^2 - x^\trans x \right)$ is concave to show that $g(\cdot, \cdot, y, u)$ is concave. We let:
	$$ X = \pmat{x \\ t} \quad J = \pmat{I_n & 0 \\ 0 & -1} \qquad \text{such that } h(X) = \log \left( - X^\trans J X \right) $$
	Now we compute the gradient of $h$ and its Hessian:
	$$ \nabla h(X) = \dfrac{2 J X}{X^\trans J X} \qquad \nabla^2 h(X) = 2 \dfrac{\left( X^\trans J X \right) J - 2 X J^\trans J X^\trans}{\left( X^\trans J X \right)^2} = 2 \dfrac{\left( X^\trans J X \right) J - 2 X X^\trans}{\left( X^\trans J X \right)^2} $$
	Now we consider a vector $Z = \pmat{ z^\trans v }^\trans$ of $\R^{n+1}$. We look at the sign of $Z^\trans \nabla^2 h(X) Z$ that is to say the sign of $Z^\trans \left( X^\trans J X J - 2 X X^\trans \right) Z$:
	$$ \begin{array}{lll}
	Z^\trans \left( X^\trans J X J - 2 X X^\trans \right) Z & = & (x^\trans x - t^2) (z^\trans z - v^2) - 2 \left[ \left( z^\trans x \right)^2 + (tv)^2 \right] \\
	& = & \| x \|_2^2 \| z \|_2^2 - t^2 \| z \|_2^2 - v^2 \| x \|_2^2 - 2 \left( z^\trans x \right)^2 - (tv)^2
	\end{array} $$
	We also now that $t > \| x \|_2$ which gives:
	$$ Z^\trans \left( X^\trans J X J - 2 X X^\trans \right) Z \leqslant - 2 v^2 \| x \|_2^2 - 2 \left( z^\trans x \right)^2 \leqslant 0 $$
	This means that $\nabla^2 h \preceq 0$. So $h$ is concave. Then $g(\cdot, \cdot, y, u)$ is concave on $K$.
	We compute the partial derivatives of $g$ with respect to $x$ and $t$:
	$$ \dfrac{\partial g}{\partial x}(x, t, y, u) = y - \dfrac{2x}{t^2 - x^\trans x} \qquad \dfrac{\partial g}{\partial t}(x, t, y, u) = u + \dfrac{2t}{t^2 - x^\trans x} $$
	Let $\alpha = \frac{1}{2} \left( t^2 - x^\trans x \right)$. Because $g(\cdot, \cdot, y, u)$ is concave, he maxima satisfies $\nabla g = 0$. That is to say:
	$$ x = \alpha y \qquad t = - \alpha u $$
	We inject this two new expressions in the derivative of $g$ with respect to $x$ and we obtain:
	$$ y = \dfrac{2 \alpha y}{\alpha^2 u^2 - \alpha^2 y^\trans y} = \frac{1}{\alpha} \dfrac{2y}{u^2 - y^\trans y} \quad \Rightarrow \quad \alpha = \dfrac{2}{u^2 - y^\trans y} \geqslant 0 $$
	We check that $(x, t)$ is in $K$. We have $\| x \|_2 = \alpha \| y \| < - \alpha u = t$, because we supposed $\| y \|_2 < -u$. We can now compute $g(x, t, y, u)$:
	$$ g(x, t, y, u) = \alpha y^\trans y - \alpha u^2 + \log \left( \alpha^2 u^2 - \alpha^2 y^\trans y \right) = -2 + \log \left( \dfrac{4}{u^2 - y^\trans y} \right) = \log 4 - 2 - \log \left( u^2 - y^\trans y \right) $$
	We are finally able to give the expression of $f^*$:
	\begin{center}
		\fbox{$\displaystyle f^*(y, u) = \left\{ \begin{array}{ll}
			+ \infty & \text{If } \| y \|_2 \geqslant -u \\
			\log 4 - 2 - \log \left( u^2 - y^\trans y \right) & \text{If } \| y \|_2 < -u
			\end{array} \right.$}
	\end{center}
	
\end{document}