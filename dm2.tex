\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Convex Optimization --- DM 2
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\diag}{\text{\textbf{diag}}~}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}

\begin{document}
	
	\maketitle
	
	\section*{Exercise 1 (LP Duality)}
	
	\paragraph{1.}
	We first compute the dual function:
	$$ g_P(\lambda, \nu) = \inf_x \left( c^\trans x - \lambda^\trans x + \nu^\trans (Ax - b) \right) = \inf_x \left( \left( c - \lambda + A^\trans \nu \right)^\trans x - \nu^\trans b \right) $$
	If the vector by which we multiply $x$ is not null then $g$ is unbounded. We then obtain:
	$$ g_P(\lambda, \nu) = \syst{ll}{
		- b^\trans \nu & \text{If } c - \lambda + A^\trans \nu = 0 \\
		- \infty & \text{Otherwise}
	} $$
	Then the dual problem can be written:
	$$ \begin{array}{llr}
		\max_{\lambda, \nu} & - b^\trans \nu \\
		\text{s.t.} & A^\trans \nu + c = \lambda \\
		& \lambda \succeq 0
	\end{array} $$
	This can be simplified by injecting $\lambda$ in the first equality constraint:
	$$ \begin{array}{llr}
	\max_{\nu} & - b^\trans \nu \\
	\text{s.t.} & A^\trans \nu + c \succeq 0
	\end{array} $$
	We can now substitute $\nu$ by $-y$ in this formulation and we exactly obtain the problem (D).
	\begin{center}
		\fbox{The dual problem of (P) is (D).}
	\end{center}
	
	\paragraph{2.}
	We first compute the dual function. We replace the objective function by its opposite because the problem is formulated with a maximum instead of a minimum. We need to keep in mind that the equivalent problem in which we replace the maximum by the minimum of the opposite function has an optimal value that is the opposite of the original problem.
	$$ g_D(\lambda) = \inf_y \left( - b^\trans y + \lambda^\trans \left( A^\trans y - c \right) \right) = \inf_y \left( \left( -b + A \lambda \right)^\trans y - \lambda^\trans c \right) $$
	If the vector by which we multiply $y$ is not null then $g$ is unbounded. We then obtain:
	$$ g_D(\lambda) = \syst{ll}{
		- c^\trans \lambda & \text{If } -b + A \lambda = 0 \\
		- \infty & \text{Otherwise}
	} $$
	Then the dual problem can be written:
	$$ \begin{array}{llr}
	\max_{\lambda} & - c^\trans \lambda \\
	\text{s.t.} & A \lambda = b \\
	& \lambda \succeq 0
	\end{array} $$
	Finally we can replace the maximum by the opposite of the minimum of the opposite objective function. Because we computed the dual of an equivalent problem that have an optimal value opposed to the original one, we retrieve the problem (P).
	\begin{center}
		\fbox{The dual problem of (D) is (P).}
	\end{center}
	
	\paragraph{3.}
	We first compute the dual function:
	$$ \begin{array}{lll}
	g(\lambda, \lambda', \nu) & = & \inf_{x, y} c^\trans x - b^\trans y - \lambda^\trans x + \lambda'^\trans \left( A^\trans y - c \right) + \nu^\trans (Ax - b) \\
	& = & \inf_{x, y} \left( c - \lambda + A^\trans \nu \right)^\trans x \, + \, \left( -b + A \lambda' \right)^\trans y \, - \, \lambda'^\trans c \, - \, \nu^\trans b \\[2mm]
	& = & \syst{ll}{
		- c^\trans \lambda' - b^\trans \nu & \text{If } c - \lambda + A^\trans \nu = 0, \; -b + A \lambda' = 0 \\
		- \infty & \text{Otherwise}
	}
	\end{array} $$
	One could also remarks that $g(\lambda, \lambda', \nu) = g_P(\lambda, \nu) + g_D(\lambda')$ which come from the fact that our new objective function is the sum of the problems (P) and (D) and that our constraints are the concatenation of the constraints of the two previous problems. We can clearly imagine that because the dual of (P) is (D) and the dual of (D) is (P), the dual of our problem will be the same sum/concatenation of (P) and (D) hence the problem we are considering is self-dual. But let justify it by writing the dual thanks to $g$ :
	$$ \begin{array}{llr}
	\max_{\lambda, \lambda', \nu} & - c^\trans \lambda' - b^\trans \nu \\
	\text{s.t.} & A^\trans \nu + c = \lambda \\
	& A \lambda' = b \\
	& \lambda \succeq 0, \; \lambda' \succeq 0
	\end{array} $$
	As in the two previous question, we replace $\lambda'$ by $x$. We inject $\lambda$ in the first equality to eliminate it. We replace $\nu$ by $-y$. And we replace the maximum by the minimum of the opposite objective function:
	$$ \begin{array}{llr}
	\min_{x, y} & c^\trans x - b^\trans y \\
	\text{s.t.} & - A^\trans y + c \succeq 0 \\
	& A x = b \\
	& x \succeq 0
	\end{array} $$
	We recognize the problem (Self-Dual).
	\begin{center}
		\fbox{The problem (Self-Dual) is self-dual.}
	\end{center}

	\paragraph{4.}
	We can remark that $x$ and $y$ are independent in the problem (Self-dual). That is to say, $x$ and $y$ don't appear at the same time in a constraint and the objective function can be split in the sum of one function depending only in $x$ and another depending only in $y$. We can then optimize $x$ and $y$ separately. The restriction of (Self-Dual) to $x$ is exactly (P) and the restriction to $y$ is (D) (by using the fact that minimizing the opposite of an expression is equivalent to maximizing). Thus:
	\begin{center}
		\fbox{The vector $[x^*, y^*]$ can also be obtained by solving (P) and (D).}
	\end{center}
%	The problem is linear because the objective function and the constraints are linear. Then we effectively have the strong duality. Because the problem is self-dual $[x^*, y^*]$ is also optimal for its dual. We recall that :
%	$$ x^* = \lambda'^* \qquad y^* = - \nu^* \qquad \lambda^* = A^\trans \nu^* + c $$
%	We then write the complementary slackness conditions :
%	$$ \left( A^\trans y^* - c \right)_i x_i^* = 0, \quad i = 1, ..., d $$
%	Now we rewrite the objective function using $Ax^* = b$ :
%	$$ f_0(x^*, y^*) = c^\trans x^* - b^\trans y^* = c^\trans x^* - (x^*)^\trans A^\trans y^* = - \left( A^\trans y^* - c \right)^\trans x^* = - \sum_{i = 1}^d \left( A^\trans y^* - c \right)_i x_i^* = 0 $$
%	We can conclude :
	By strong duality of linear programs we have $p^* = d^*$ where $p^* = c^\trans x^*$ is the optimal value of (P) and $d^* = b^\trans y^*$ is the optimal value of (D). Then the value of $[x^*, y^*]$ in (Self-Dual) is $p^* - d^* = 0$.
	\begin{center}
		\fbox{The optimal value of (Self-Dual) is exactly 0.}
	\end{center}
	
	\section*{Exercise 2 (Regularized Least-Square)}
	
	\paragraph{1.}
	Let $g(x, y) = y^\trans x - \| x \|_1$. We denote by $\| . \|_1^*$ the conjugate of $\| . \|_1 $ such that: 
	$$ \| y \|_1^* = \sup_x g(x, y) $$
	Suppose that there exists $i$ such that $| y_i | > 1$. Then by denoting $e_i$ the vector with a one at position $i$ and zeros everywhere else, for $\lambda > 0$, we have :
	$$ g(\lambda e_i, y) = \lambda \left( | y_i | - 1 \right) \xrightarrow[\lambda \rightarrow + \infty]{} +\infty $$
	So $\| y \|_{\infty} > 1$ implies $\| y \|_1^* = + \infty$. \\
	Now suppose $\| y \|_{\infty} \leqslant 1$. In this case $x_i y_i \leqslant | x_i |$, then :
	$$ g(x, y) = \sum_{i = 1}^d x_i y_i - | x_i | \leqslant 0 $$
	We deduce $\| y \|_1^* \leqslant 0$. Because $g(0, y) = 0$ this is an equality. We finally have :
	\begin{center}
		\fbox{$ \displaystyle \| y \|_1^* = \syst{ll}{
			0 & \text{If } 	\| y \|_{\infty} \leqslant 1 \\
			+ \infty & \text{Otherwise}
		} $}
	\end{center}

	\paragraph{2.}
	We introduce the new variable $y = Ax + b$ in the problem to obtain :
	$$ \begin{array}{llr}
		\min_{x, y} & \| y \|_2^2 + \| x \|_1 \\
		\text{s.t.} & Ax + b = y
	\end{array} $$
	We then compute the dual function :
	$$ \begin{array}{lll}
	g(\nu) & = & \inf_{x, y} \| y \|_2^2 - \nu^\trans y + \nu^\trans A x + \| x \|_1 + \nu^\trans b \\[2mm]
	& = & - \| \nu \|_2^{2 \, *} - \left\| - A^\trans \nu \right\|_1^* + \nu^\trans b \\[2mm]
	& = & \syst{ll}{
		\nu^\trans b - \| \nu \|_2^{2 \, *} & \text{If } \, \left\| A^\trans \nu \right\|_{\infty} \leqslant 1 \\
		- \infty & \text{Otherwise}
	}
	\end{array} $$
	Now we compute the conjugate of the square of the 2-norm :
	$$ \| \nu \|_2^{2 \, *} = \max_y \nu^\trans y - \| y \|_2^2 = \max_y h(\nu, y) $$
	Where $h$ is a strictly concave function. We compute the gradient of $h$ with respect to $y$ :
	$$ \nabla_y h(\nu, y) = \nu - 2 y $$
	Then $h$ is maximal for $y = \frac{1}{2} \nu$ and we obtain :
	\begin{equation}
		\label{conj2}
		\| \nu \|_2^{2 \, *} = \frac{1}{4} \| \nu \|_2^2
	\end{equation}
	Then our dual can be written :
	\begin{center}
		\fbox{$ \displaystyle \text{(Dual RLS)} \quad \begin{array}{llr}
			\max_\nu & b^\trans \nu - \frac{1}{4}\| \nu \|_2^2 \\[2mm]
			\text{s.t.} & \left\| A^\trans \nu \right\|_{\infty} \leqslant 1
			\end{array} $}
	\end{center}

	\section*{Exercise 3 (Data Separation)}
	
	\paragraph{1.}
	First, we can remark that (Sep. 2) is feasible by simply taking the point $(0, \mathbf{1})$ and bounded because the objective function is non-negative for $z \succeq 0$. \\
	Let $(\omega, z)$ a feasible point. Suppose that there exists $i$ such that $z_i > \mathcal{L}(\omega, x_i, y_i)$. Then we let $z'$ such that $z'_j = z_j$ for $j \neq i$ and $z'_i = \mathcal{L}(\omega, x_i, y_i)$. We have in particular : \vspace{-2mm}
	$$ z'_i \geqslant 0 \qquad z'_i \geqslant 1 - y_i (\omega^\trans x_i) $$
	Furthermore the other constraints are also verified because $z$ is feasible. Because the factor of $z_i$ is strictly positive in the objective function and because $z'_i < z_i$, the value associated to the point $(\omega, z')$ is strictly inferior to the value of $(\omega, z)$. \\
	Then for $(\omega^*, z^*)$ optimal we have $z_i^* \leqslant \mathcal{L}(\omega^*, x_i, y_i)$ for every $i$. Furthermore, because of constraints, this inequality is in fact an equality. So : \vspace{-2mm}
	$$ z_i^* = \mathcal{L}(\omega^*, x_i, y_i) $$
	In this case the optimal value is :
	$$ p_2^* = \dfrac{1}{n \tau} \sum_{i = 1}^n \mathcal{L}(\omega^*, x_i, y_i) + \dfrac{1}{2} \| \omega^* \|_2^2 = \dfrac{1}{\tau} p_1(\omega^*) $$
	Where $p_1(\omega)$ is the value of the point $\omega$ in the problem (Sep. 1). \\
	Finally we can remark that for every $\omega$, the point $\left( \omega, \left( \mathcal{L}(\omega, x_i, y_i) \right)_{i = 1}^n \right)$ is feasible for the problem (Sep. 2). We showed before that the optimal value can be expressed in this form then:
	$$ p_2^* = \inf_\omega \dfrac{1}{\tau} p_1(\omega) = \dfrac{1}{\tau} p_1^* = \dfrac{1}{\tau} p_1(\omega^*) $$
	We can now conclude:
	\begin{center}
		\fbox{\parbox{0.93\linewidth}{\centering
			The problem (Sep. 2) as a solution $(\omega^*, z^*)$ of value $p_2^*$ such that $\omega^*$ is optimal for (Sep. 1), $z^*$ is the vector of losses and $p_1^* = \tau p_2^*$.
		}}
	\end{center}
	
	\paragraph{2.}
	We first compute the dual function :
	$$ \begin{array}{lll}
	g(\omega, z, \lambda, \pi) & = & \inf_{\omega, z} \dfrac{1}{n \tau} \mathbf{1}^\trans z + \dfrac{1}{2} \| \omega \|_2^2 + \mathbf{1}^\trans \lambda  - \lambda^\trans \diag(y) X \omega - \lambda^\trans z - \pi^\trans z \\
	& = & \mathbf{1}^\trans \lambda - \dfrac{1}{2} \left\| 2 X^\trans \diag(y) \lambda \right\|_2^{2 \, *} + \inf_z \left( \dfrac{1}{n \tau} \mathbf{1} - \lambda - \pi \right)^\trans z
	\end{array} $$
	Where $X \in \R^{n \times d}$ is the matrix with $x_i^\trans$ as $i$-th row and $ \| . \|_2^{2 \, *}$ is the conjugate of the 2-norm as seen in the previous exercise (eq \eqref{conj2}):
	$$ X = \pmat{x_1^\trans \\ \vdots \\ x_n^\trans } \qquad \| v \|_2^{2 \, *} = \dfrac{1}{4} \| v \|_2^2 $$
	We then obtain:
	$$ g(\omega, z, \lambda, \pi) = \syst{ll}{
		\mathbf{1}^\trans \lambda - \dfrac{1}{2} \left\| X^\trans \diag(y) \lambda \right\|_2^2 & \text{If } \; \lambda = \frac{1}{n \tau} \mathbf{1} - \pi \\[2mm]
		- \infty & \text{Otherwise}
	} $$
	This gives the following dual problem:
	$$ \begin{array}{llr}
	\max_{\lambda, \pi} & \mathbf{1}^\trans \lambda - \dfrac{1}{2} \left\| X^\trans \diag(y) \lambda \right\|_2^2 \\[2mm]
	\text{s.t.} & \lambda = \frac{1}{n \tau} \mathbf{1} - \pi \\[1mm]
	& \lambda \succeq 0, \; \pi \succeq 0
	\end{array} $$
	We can inject the inequality on $\pi$ in the first equality and remove this variable from the dual:
	\begin{center}
		\fbox{$ \text{(Dual Sep. 2)} \quad \begin{array}{llr}
			\max_{\lambda} & \mathbf{1}^\trans \lambda - \dfrac{1}{2} \left\| X^\trans \diag(y) \lambda \right\|_2^2 \\[2mm]
			\text{s.t.} & 0 \preceq \lambda \preceq \frac{1}{n \tau} \mathbf{1}
			\end{array} $}
	\end{center}
	
	\section*{Exercise 4 (Robust linear programming)}
	
	\paragraph{}
	We denote by $p^*(x)$ the value:
	$$ p^*(x) = \sup_{a \in \mathcal{P}} a^\trans x = - \inf_{a \in \mathcal{P}} - a^\trans x $$
	Then $p^*(x)$ is the opposite of the optimal value of a linear program. We know that strong duality holds in this case and we compute the dual function associated to (P) $\inf_{a \in \mathcal{P}} - a^\trans x$:
	$$ g(a, z) = \inf_a -a^\trans x + z^\trans C^\trans a - z^\trans d = \syst{ll}{
		-z^\trans d & \text{If } \, Cz = x \\
		- \infty & \text{Otherwise}
	} $$
	This gives the following dual of (P):
	$$ (D) \quad \begin{array}{llr}
	\max_z & - d^\trans z \\
	\text{s.t.} & Cz = x \\
	& z \succeq 0
	\end{array} $$
	The optimal value of this dual is $d^*(x) = -p^*(x)$ by strong duality.
	
	\paragraph{}
	Because the objective function of the two LP we want to show the equivalence are the same, it is sufficient to show that :
	\begin{equation}
		\label{equiv}
		p^*(x) \leqslant b \; \Leftrightarrow \; \exists z \succeq 0, \, C z = x \text{ and } d^\trans z \leqslant b
	\end{equation}
	\textit{Remark that I wrote $Cz =x$ instead of $C^\trans z = x$ as suggested in the exercise statement, which is certainly wrong} \\
	Now we suppose that $x$ satisfies $p^*(x) \leqslant b$. We know that there exists $z^* \succeq 0$ such that:
	$$ C z^* = x \qquad \text{And} \qquad - d^\trans z^* = d^*(x) = - p^*(x) \geqslant -b \; \Leftrightarrow \; d^\trans z^* \leqslant b $$
	Inversely, now we suppose that $p^*(x) > b$. Let $z \succeq 0$ such that $Cz = x$. Thus, $z$ is in the polytope of (D). This gives :
	$$ - d^\trans z \leqslant d^*(x) = -p^*(x) < -b \; \Leftrightarrow \; d^\trans z > b $$
	Then there is no $z \succeq 0$ such that $C z = x \text{ and } d^\trans z \leqslant b$. \\
	We just proved the equivalence \eqref{equiv} that allows us to conclude :
	\begin{center}
		\fbox{$ \displaystyle \begin{array}{llr}
			\min_x & c^\trans x \\
			\text{s.t.} & \sup_{a \in \mathcal{P}} a^\trans x \leqslant b
			\end{array} \qquad \Leftrightarrow \qquad \begin{array}{llr}
			\min_{x, z} & c^\trans x \\
			\text{s.t.} & d^\trans z \leqslant b \\
			& Cz = x \\
			& z \succeq 0
			\end{array} $}
	\end{center}

	\subsection*{Exercise 5 (Boolean LP)}
	
	\paragraph{1.}
	We first compute the Lagrangian of our problem: \vspace{-3mm}
	$$ L(x, \lambda, \nu) =  c^\trans x + \lambda^\trans (Ax - b) + \sum_{i = 1}^n \nu_i x_i (1 - x_i) \vspace{-2mm} $$
	Now we compute the gradient with respect to $x_i$ of the Lagrangian:
	$$ \nabla_{x_i} L(x, \lambda, \nu) = c_i + a_i^\trans \lambda + \nu_i (1 - 2 x_i) \qquad \text{where } \; A = \pmat{a_1 & \cdots & a_n} $$
	This gradient depends only on $x_i$ then the minimum of $L$ w.r.t $x$ is obtained by optimizing on each $x_i$ independently. We can remark that if $\nu_i > 0$, then $L(\alpha e_i, \lambda, \nu)$ tends to $- \infty$ when $\alpha$ tends to $+\infty$. So, for $L$ to be bounded, we need $\nu \preceq 0$. In this case $L$ is convex in $x$ and we just need to find the vanishing point of the gradient to obtain the minimum of $L$:
	$$ \nabla_{x_i} L(x, \lambda, \nu) = 0 \quad \Leftrightarrow \quad \left( \nu_i < 0 \, \text{ and } \, x_i = \dfrac{c_i + a_i^\trans \lambda + \nu_i}{2 \nu_i} \right) \; \text{or} \; \left( \nu_i = 0 \, \text{ and } \, c_i + a_i^\trans \lambda = 0 \right) $$
	Now we write:
	$$ L(x, \lambda, \nu) = - \lambda^\trans b + \sum_{i = 1}^n L_i(x_i, \lambda, \nu_i) \qquad \text{where } \; L_i(x_i, \lambda, \nu_i) = \left( c_i + a_i^\trans \lambda + \nu_i - \nu_i x_i \right) x_i $$
	Such that the dual function wan be written:
	$$ g(\lambda, \nu) = \inf_x L(x, \lambda, \nu) = - \lambda^\trans b + \sum_{i = 1}^n \inf_{x_i} L_i(x_i, \lambda, \nu_i) = -\lambda^\trans b + \sum_{i = 1}^n g_i(\lambda, \nu_i) $$
	With the following expression of $g_i$, from what we have done before:
	$$ g_i(\lambda, \nu_i) = \syst{ll}{
		L_i \left( \dfrac{c_i + a_i^\trans \lambda + \nu_i}{2 \nu_i}, \, \lambda, \, \nu_i \right) & \text{If } \; \nu_i < 0 \\
		0 & \text{If } \; \nu_i = 0 \; \text{ and } \; c_i + a_i^\trans \lambda = 0 \\
		- \infty & \text{Otherwise}
	} $$
	We simplify the case $\nu_i < 0$: \vspace{-3mm}
	$$ \nu_i < 0 \: \Rightarrow \; g_i(\lambda, \nu_i) = \dfrac{1}{4 \nu_i} \left( c_i + a_i^\trans \lambda + \nu_i \right)^2 $$
	Because $g$ depends on $\nu_i$ only in the term $g_i(\lambda, \nu_i)$ we can simplify the expression of the Lagrange dual by maximizing each $g_i$ with respect to $\nu_i$ and removing each $\nu_i$ from the dual variables. \\
	From the hint given in the question we have:
	$$ \sup_{\nu_i < 0} g_i(\lambda, \nu_i) = \min \left\{ 0, \, c_i + a_i^\trans \lambda \right\} $$
	Furthermore :
	$$ c_i + a_i^\trans \lambda = 0 \quad \Rightarrow \quad g_i(\lambda, 0) = 0 = \min \left\{ 0, \, c_i + a_i^\trans \lambda \right\} $$
	Now we can remove the variable $\nu_i$ to obtain:
	$$ g_i(\lambda) = \sup_{\nu_i} g_i(\lambda, \nu_i) = \min \left\{ 0, \, c_i + a_i^\trans \lambda \right\} $$
	Finally we obtain the following expression for the Lagrange dual:
	\begin{center}
		\fbox{$ \displaystyle g(\lambda) = -b^\trans \lambda + \sum_{i = 1}^n \min \left\{ 0, \, c_i + a_i^\trans \lambda \right\} $}
	\end{center}

	\paragraph{2.}
	We compute the Lagrange dual of the LP relaxation:
	$$ h(x, \alpha, \beta, \gamma) = \inf_x \; c^\trans x + \alpha^\trans Ax - \alpha^\trans b - \beta^\trans x + \gamma^\trans x - \gamma^\trans \mathbf{1} = \syst{ll}{
		- \alpha^\trans b - \gamma^\trans \mathbf{1} & \text{If } \; c + A^\trans \alpha + \gamma = \beta \\
		- \infty & \text{Otherwise}
	} $$
	This gives the follwing dual problem:
	$$ \begin{array}{llr}
	\max_{\alpha, \beta, \gamma} & - \alpha^\trans b - \gamma^\trans \mathbf{1} \\
	\text{s.t.} & c + A^\trans \alpha + \gamma = \beta \\
	& \alpha, \beta, \gamma \succeq 0
	\end{array} $$
	We can inject the non-negativity of $\beta$ in the equality constraint and remove this variable:
	$$ \begin{array}{llr}
	\max_{\alpha, \gamma} & - \alpha^\trans b - \gamma^\trans \mathbf{1} \\
	\text{s.t.} & c + A^\trans \alpha + \gamma \succeq 0 \\
	& \alpha, \gamma \succeq 0
	\end{array} $$
	We also remark that the objective function is strictly decreasing with respect to $\gamma$. Then the optimal is obtain for $\gamma$ as small as possible when $\alpha$ is fixed. This leads us to:
	$$ \gamma_i  = \max \left\{ 0, \, - c_i - a_i^\trans \alpha \right\} = - \min \left\{ 0, \, c_i + a_i^\trans \alpha \right\} $$
	Now we are able to simplify again our dual:
	$$ \text{(D2)} \quad \begin{array}{llr}
	\max_{\alpha} & g(\alpha) = - \alpha^\trans b + \sum_{i = 1}^n \min \left\{ 0, \, c_i + a_i^\trans \alpha \right\} \\
	\text{s.t.} & \alpha \succeq 0
	\end{array} $$
	We recognize the dual obtain in the previous question for the Lagrange relaxation.
	\begin{center}
		\fbox{The lower bound obtained via Lagrangian relaxation, and via the LP relaxation (2), are
			the same.}
	\end{center}
	
\end{document}