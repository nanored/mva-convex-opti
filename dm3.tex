\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Convex Optimization --- DM 3
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\diag}{\text{\textbf{diag}}~}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\one}{\mathbbm{1}}

\begin{document}
	
	\maketitle
	
	\section*{Question 1}
	
	We transform our primal by adding a new variable in order to add a constraint:
	$$ \begin{array}{llr}
		\min_{w, z} & \dfrac{1}{2} \| z \|_2^2 + \lambda \| w \|_1 \\[2mm]
		\text{s.t.} & X w - y = z
	\end{array} $$
	Then, we compute the dual function of this new formulation:
	$$ \begin{array}{lll}
		g(v) & = & \inf_{z, w} \dfrac{1}{2} \| z \|_2^2 + \lambda \| w \|_1 + v^\trans \left( Xw - y - z \right) \\[2mm]
		& = & - v^\trans y + \inf_{z, w} - v^\trans z + \dfrac{1}{2} \| z \|_2^2 + \left( X^\trans v \right)^\trans w + \lambda \| w \|_1 \\[3mm]
		& = & - v^\trans y - \dfrac{1}{2} \| 2 v \|_2^{2 \, *} - \lambda \left\| - \dfrac{1}{\lambda} X^\trans v \right\|_1^*
	\end{array} $$
	In the previous homework I have computed the conjugate of the square of 2-norm and the conjugate of the 1-norm. We have :
	$$ \| x \|_2^{2 \, *} = \frac{1}{4} \| x \|_2^2 \qquad \| x \|_1^* = \syst{ll}{
		0 & \text{If } 	\| x \|_{\infty} \leqslant 1 \\
		+ \infty & \text{Otherwise}
	} $$
	We then simplify the dual function:
	$$ g(v) = \syst{ll}{
		- \dfrac{1}{2} \| v \|_2^2 - y^\trans v & \text{If } \left\| X^\trans v \right\|_\infty \leqslant \lambda \\
		- \infty & \text{Otherwise} 
	} $$
	Furthermore we can rewrite the condition by :
	$$ \forall i = 1, ..., d \; \left| \left( X^\trans v \right)_i \right| \leqslant \lambda \Leftrightarrow X^\trans v \preceq \lambda \one_d \text{ and } - X^\trans v \preceq \lambda \one_d $$
	Then we let the matrix:
	$$ A = \pmat{X^\trans \\ -X^\trans} \qquad \text{such that } A v \preceq \lambda \one_{2d} $$
	We also remark the following equality:
	$$ \dfrac{1}{2} \| v \|_2^2 = \dfrac{1}{2} v^\trans I_n v = v^\trans Q v \qquad \text{with } Q = \dfrac{1}{2} I_n $$
	The dual problem is obtained by maximizing $g$ which is equivalent to minimizing $-g$. Thus we obtain the following dual:
	$$ (QP) \qquad \begin{array}{llr}
		\min_v & v^\trans Q v + y^\trans v \\[2mm]
		\text{s.t.} & A v \preceq \lambda \one_{2d}
	\end{array} $$
	We identify with the notations of the statement :
	\begin{center}
		\fbox{$ \displaystyle Q = \dfrac{1}{2} I_n \quad p = y \quad A = \pmat{X^\trans \\ -X^\trans} \quad b = \lambda \one_{2d} $}
	\end{center}

	\section*{Question 2}
	
	\paragraph{Gradient and Hessian}
	To implement the centering step we need to compute the gradient and the Hessian matrix of our approximated objective function:
	$$ f_t(v) = t \left( v^\trans Q v + p^\trans v \right) - \sum_{i = 1}^{2d} \log \left( b_i - a_i^\trans v \right) $$
	Where $a_i$ is the $i$-th line of $A$. To simplify the expression, I replace $Q$ and $b$ by their values:
	$$ f_t(v) = t \left( \dfrac{1}{2} v^\trans v + p^\trans v \right) - \sum_{i = 1}^{2d} \log \left( \lambda - a_i^\trans v \right) $$
	Firstly, we obtain the following gradient:
	\begin{center}
		\fbox{$ \displaystyle \nabla f_t(v) = t (v + p) + \sum_{i = 1}^{2d} \dfrac{a_i}{\lambda - a_i^\trans v} $}
	\end{center}
	Then here is the Hessian matrix:
	\begin{center}
		\fbox{$ \displaystyle \nabla^2 f_t(v) = t I_n + \sum_{i = 1}^{2d} \dfrac{a_i a_i^\trans}{\left( \lambda - a_i^\trans v \right)^2	} $}
	\end{center}
	
	\paragraph{Newton stopping criteria}
	We also need to know when we can stop the Newton method to obtain a target precision $\epsilon$. To do so, we remark the following inequality:
	$$ \nabla^2 f_t(v) \succeq t I_n $$
	Now we let $m = t$, a constant of strict convexity of $f_t$ and as seen in class, we have the inequality:
	$$ f_t(v) - p^*(t) \leqslant \dfrac{1}{2m} \| \nabla f_t(v) \|_2^2 $$
	Then our stopping criteria inside the Newton method is :
	\begin{center}
		\fbox{$\displaystyle \| \nabla f_t(v) \|_2^2 \leqslant 2 m \epsilon = 2 t \epsilon$}
	\end{center}

	\paragraph{Initial point}
	For $\lambda > 0$ it is easy to remark that the point $0$ is strictly feasible. Then we decide to start with this initial value $v_0 = 0$.

	\section*{Question 3}
	
	First, here is a figure representing the gap at each iteration for different values of $\mu$:
	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{conv.png}
		\caption{Gap in function of iterations for several $\mu$}
	\end{figure}

	We can remark that when $\mu$ increases, the number of iterations until convergence decreases. Furthermore, the computational time decreases with $\mu$ as the number of iterations decreases. In that way, for $\mu = 2$, the computation time was about 9s on my laptop, whereas for the three other values the computation time was between 2.5s and 3.5s. \\
	Finally we can observe that at the end of the algorithm the gap is smaller when $\mu$ is small. But in every cases the gap is effectively smaller than $\epsilon$ as wanted.
	
	\paragraph{Primal variable}
	In order to check the impact of $\mu$ on $w$ we need to compute $w$ from the dual variable $v$. To do so we recall the Laplacian :
	$$ L(w, z, v) = \dfrac{1}{2} \| z \|_2^2 + \lambda \| w \|_1 + v^\trans \left( Xw - y - z \right) $$
	If we derive it with respect to $z$ we obtain:
	$$ \dfrac{\partial L}{\partial z} = z - v \quad \Rightarrow \quad z^* = v^* $$
	Then we use the equality:
	$$ Xw - y = z $$
	We multiply on the left by $X^\trans$ to obtain:
	$$ X^\trans X w^* = X^\trans (y + v^*) $$
	Then if our matrix $X^\trans X$ is inversible we get:
	\begin{center}
		\fbox{$ \displaystyle w^* = \left( X^\trans X \right)^{-1} X^\trans (y + v^*) $}
	\end{center}
	Unfortunately, I've certainly done something wrong because the primal value obtained by taking this value for $w$ is different from the dual value (about twice as large). I've checked that the LASSO algorithm from \verb|sklearn| library gives me the same value as I obtained from the barrier method. And it is effectively the same value, so my barrier method seems to be right, but my formula to obtain $w$ from $v$ is probably wrong. Thus I could not observed the effects+ of $\mu$ on $w$. I observed that the L1 norm of $w$ slightly decreases with $\mu$ but, because my formula to obtain $w$ is certainly wrong, it is probably an incorrect observation.
	
	
\end{document}\texttt{}