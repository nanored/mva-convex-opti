\chapter{Algorithmes}

\myminitoc

\sect{Minimisation sans contraintes}

Dans la suite on considère des problèmes sans contraintes. C'est à dire qu'on a simplement :
$$ \min_x f(x) $$
On supposera $f$ convexe, deux fois différentiable (et donc $\dom f$ ouvert). Enfin on supposera que $p^*$ est atteint et est fini. On construira des suites $(x^{(k)})_{k \geqslant 0}$ à valeurs dans $\dom f$ dont l'image par $f$ converge vers $p^*$. De plus on supposera toujours que l'ensemble $S$ est fermé, où S est défini par :
$$ S = \left\{ x \mid f(x) \leqslant f \left( x^{(0)} \right) \right\} $$

\subs{Méthode de descente générale}

Voici un algorithme de descente dans un cadre très générale

\begin{algorithm}[h]
	\caption{Méthode de descente générale}
	\KwIn{Un point de départ $x \in \dom f$}
	\Repeat{Un critère d'arrêt est vérifié}{
		Déterminer une direction de descente $\Delta x$ \\
		Choisir une longueur d'étape $t > 0$ \\
		Mettre à jour $x$ via : $x \gets x + t \Delta x$
	}
\end{algorithm}

Comme on veut $f \left( x^{(k+1)} \right) < f \left( x^{(k)} \right)$ il faut que la direction $\Delta x$ soit dans le demi-espace :
$$ \nabla f(x)^\trans \Delta x < 0 $$

\paragraph{Critère d'arrêt}
Si $f$ est strictement convexe et que l'on connaît un $m > 0$ tel que :
$$ \forall x \in S, \; \nabla^2 f(x) \succeq m I $$
Alors on a en utilisant la convexité :
$$ f(y) \geqslant f(x) + \nabla f(x)^\trans (y - x) + \dfrac{m}{2} \| y - x \|_2^2 $$
En prenant $y = x^*$, cette équation se réécrit :
$$ f(x) - p^* \leqslant - \nabla f(x)^\trans (x^* - x) - \dfrac{m}{2} \| x^* - x \|_2^2 $$
On cherche un majorant du côté droit de l'inégalité. Pour cela on écrit l'équation d'annulation du gradient en $y$ et on obtient :
$$ y = x - \dfrac{1}{m} \nabla f(x) $$
Ce qui donne finalement :
\begin{equation}
	\label{stopcont}
	f(x) - p^* \leqslant \dfrac{1}{2 m} \left\| \nabla f(x) \right\|_2^2
\end{equation}
Si on est capable de calculer le gradient on peut donc connaître une borne supérieur sur la distance avec la valeur optimale.

\paragraph{Recherche de ligne par \textit{backtracking}}
Idéalement on veut $t = \argmin_{t > 0} f(x + t \Delta x)$. La technique présenté dans ce paragraphe consiste à considérer $t$ de plus en plus petit jusqu'à ce qu'il vérifie une condition. Prenons alors deux paramètres $\alpha \in ]0, 1/2 [$ et $\beta \in ]0, 1[$. Voici alors l'algo :
\begin{algorithm}[h]
	\caption{Recherche de ligne par \textit{backtracking}}
	$t \gets 1$ \\
	\Repeat{$f(x + t \Delta x) < f(x) + \alpha t \nabla f(x)^\trans \Delta x$}{
		$t \gets \beta t$
	}
\end{algorithm}
\begin{center}
	\begin{tikzpicture}
		\draw[->] (0, 0) -- (6, 0) node[right] {$t$};
		\draw[->] (0, 0) -- (0, 3);
		\draw[red, thick, domain=0:5.5, smooth, variable=\x] plot ({\x}, {2.3 + 0.25*\x*(\x - 5.2)}) node[right] {$f(x + t \Delta x)$};
		\draw[dashed] (-0.1, 2.43) -- node[above right, pos=0.9] {\small $f(x) + t \nabla f(x)^\trans \Delta x$} (2, -0.3) ;
		\draw[dashed, blue, thick] (-0.1, 2.33) --  (6, 0.5) node[right] {$f(x) + \alpha t \nabla f(x)^\trans \Delta x$};
		\node[blue] at (4, 0) {\small $|$};
		\node[blue, below] at (4, 0) {$t_0$};
	\end{tikzpicture}
\end{center}

\paragraph{Direction}
Une direction classique à prendre est $\Delta x = - \nabla f(x)$.

\subs{Méthode de Newton}

On cherche à minimiser l'approximation du second ordre suivante :
$$ f(x + v) \approx \hat{f}(x + v) = f(x) + \nabla f(x)^\trans v + \dfrac{1}{2} v^\trans \nabla^2 f(x) v $$
On annule alors le gradient en $v$ pour obtenir :
$$ \Delta x_{nt} = - \nabla^2 f(x)^{-1} \nabla f(x) $$
Si on veut un critère d'arrêt, on peut remplacer l'optimal $p^*$ dans \eqref{stopcont} par une approximation avec le minimum de $\hat{f}$, c'est à dire :
$$ f(x) - \inf_y \hat{f}(y) = f(x) - \hat{f} \left( \Delta x_{nt} \right) = \dfrac{1}{2} \nabla f(x)^\trans \nabla^2 f(x)^{-1} \nabla f(x) $$
Cela donne alors le critère d'arrêt :
$$ \nabla f(x)^\trans \Delta x_{nt} \leqslant \epsilon $$
On peut alors obtenir une borne sur le nombre d'étapes nécessaires pour atteindre ce critère avec le théorème suivant.

\vspace{2mm}
\PROP{
	Si $f$ est strictement convexe de constante $m$ et $\nabla^2 f$ est $L$-Lipschitzienne avec $L > 0$. Alors il existe deux constantes $\eta \in ]0, m^2 / L [$ et $\gamma > 0$ tel que :
	$$ \left\| \nabla f(x) \right\|_2 \geqslant \eta \; \Rightarrow \; f \left( x^{(k+1)} \right) - f \left( x^{(k)} \right) \leqslant - \gamma \vspace{-2mm}$$
	$$ \left\| \nabla f(x) \right\|_2 < \eta \; \Rightarrow \; \dfrac{L}{2m^2} \left\| \nabla f \left( x^{(k+1)} \right) \right\|_2 \leqslant \left( \dfrac{L}{2m^2} \left\| f \left( x^{(k)} \right) \right\|_2 \right)^2 $$
	\vspace{-3mm}
}

\paragraph{Phase amortie}
Généralement on utilise le \textit{backtracking} pour ces étapes durant lesquels notre valeur diminue d'au moin $\gamma$ à chaque étape. Si la valeur optimale est finie alors le nombre nécessaire d'étapes est au plus $\left( f \left( x^{(0)} \right) - p^* \right) / \gamma$.

\paragraph{Phase quadratique}
Généralement dans cette phase on utilise toujours $t = 1$. De plus si $\left\| \nabla f(x) \right\|_2 < \eta$ alors :
$$ l \geqslant k \;\Rightarrow \; \dfrac{L}{2m^2} \left\| \nabla f \left( x^{(l)} \right) \right\|_2 \leqslant \left( \dfrac{L}{2m^2} \left\| f \left( x^{(k)} \right) \right\|_2 \right)^{2^{l-k}} \leqslant \left( \dfrac{1}{2} \right)^{2^{l-k}} $$

\paragraph{Conclusion}
Ainsi le nombre d'itérations avant que $f(x) - p^* \leqslant \epsilon$ est inférieur à :
$$ \dfrac{f \left( x^{(0)} \right) - p^*}{\gamma} + \log_2 \log_2 \left( \epsilon_0 / \epsilon \right) $$
Où $\epsilon_0$ dépend de $x^{(0)}$, $m$ et $L$.

\DEF{
	Une fonction $f : \R \rightarrow \R$ est dite \textbf{auto-concordante} si pour tout $x \in \dom f$ :
	$$ \left| f^{(3)}(x) \right| \leqslant 2 f^{(2)}(x)^{3/2} $$
	Pour $f : \R^n \rightarrow \R$ on dira de même si $g(t) = f(x + tv)$ est auto-concordante pour tout $v \in \R^n$ et $x \in \dom f$.
}

On remarquera que les fonction auto-concordante sont stables par composition avec des fonctions affines.

\sect{Méthode barrière}

On se donne le problème :
$$ \begin{array}{llr}
\min & f_0(x) \\
\text{t.q.} & f_i(x) \leqslant 0, & i = 1, ..., m \\
& Ax = b
\end{array} $$
Où les fonctions $f_i$ sont deux fois différentiable et $A$ est de rang maximal (i.e $\rank A = p$). On suppose que $p^*$ est fini et atteint. Enfin on suppose que le problème est strictement admissible. \\
On commence par reformuler le problème pour enlever les contraintes d'inégalités :
$$ \begin{array}{llr}
\min & f_0(x) + \sum_{i = 1}^m I_-(f_i(x)) \\
\text{t.q.} & Ax = b
\end{array} $$
Où la fonction $I_-$ est la fonction indicatrice de $\R_-$ qui vaut 0 sur les valeurs négatives et $+\infty$ sur les valeurs strictement positives. Le problème est que cette fonction indicatrice n'est pas différentiable. Pour y remédier on utilise des approximations. On pose une constante $t > 0$ et on défini le nouveau problème suivant :
$$ \begin{array}{llr}
\min & \displaystyle f_0(x) - \dfrac{1}{t} \sum_{i = 1}^m \log \left( - f_i(x) \right) \\
\text{t.q.} & Ax = b
\end{array} $$
L'approximation est meilleur lorsque $t$ tend vers l'infini.
\begin{center}
	\begin{tikzpicture}[yscale=0.25, xscale=1.25]
		\draw (-3, -5) rectangle (1, 10);
		\node[below] at (-1, -5) {-1};
		\node[below] at (-3, -5) {-3};
		\node[below] at (1, -5) {1};
		\node[left] at (-3, -5) {-5};
		\node[left] at (-3, 10) {10};
		\draw[dashed, thick]  (-3, 0) node[left] {0} -- (0, 0) -- (0, 10);
		\draw[thick, red, smooth, domain=-1.1:10, variable=\y] plot ({-exp(-\y)}, \y);
		\draw[thick, blue, smooth, domain=-2.2:10, variable=\y] plot ({-exp(-0.5*\y)}, \y);
		\draw[thick, green, smooth, domain=-4.4:10, variable=\y] plot ({-exp(-0.25*\y)}, \y);
		\node[green] at (0.5, 8.5) {\small $t = 1/4$};
		\node[blue] at (0.5, 7) {\small $t = 1/2$};
		\node[red] at (0.5, 5.5) {\small $t = 1$};
	\end{tikzpicture}
\end{center}
On note $\phi$ l'approximation utilisée :
$$ \phi(x) = - \sum_{i = 1}^m \log \left( - f_i(x) \right) $$
Par la règle des compositions, $\phi$ est bien convexe. De plus, on a la double différentiabilité :
$$ \nabla \phi(x) = - \sum_{i = 1}^m \dfrac{1}{f_i(x)} \nabla f_i(x) $$
$$ \nabla^2 \phi(x) = \sum_{i = 1}^m \dfrac{1}{f_i(x)^2} \nabla f_i(x) \nabla f_i(x)^\trans - \dfrac{1}{f_i(x)} \nabla^2 f_i(x) $$
On note aussi $x^*(t)$ la solution de notre approximation. On peut montrer que cette solution existe et est unique. De plus on sait qu'il y a optimalité si et seulement si il existe $w$ tel que :
\begin{equation}
	t \nabla f_0(x) - \sum_{i = 1}^m \dfrac{1}{f_i(x)} \nabla f_i(x) 
	A^\trans w = 0 \qquad \text{et} \quad Ax = b
	\label{opti_cond}
\end{equation}
On pose a alors les variables duals suivantes :
$$ \lambda_i^*(t) = - \dfrac{1}{t f_i(x^*(t))} \qquad \nu_i^*(t) = \dfrac{1}{t} w $$
Et alors la condition \eqref{opti_cond} se réécrit :
$$ \nabla L(x^*(t), \lambda^*(t), \nu^*(t)) = 0 \qquad A x^*(t) = b $$
Où $L$ est le Lagrangien du problème initiale, i.e. :
$$ L(x, \lambda, \nu) = f_0(x) + \sum_{i = 1}^m \lambda_i f_i(x) + \nu^\trans (Ax - b) $$
Cela nous permet alors d'avoir une borne sur notre approximation de $p^*$ :
$$ p^* \geqslant g(\lambda^*(t), \nu^*(t)) = L(x^*(t), \lambda^*(t), \nu^*(t)) = f_0 \left( x^*(t) \right) - \dfrac{m}{t} $$
De plus cette inégalité montre la convergence vers une solution optimale lorsque $t$ tend vers l'infini. \\
Finalement cela nous permet de définir l'algorithme suivant :

\begin{algorithm}[h]
	\caption{Méthode barrière}
	\KwIn{Un point de départ $x=x^{(0)}$ strictement admissible, $t=t^{(0)} > 0$, $\mu > 1$ et $\epsilon > 0$}
	\Repeat{$m/t < \epsilon$}{
		$t \gets \mu t$ \\
		\textit{Etape de centrage} : Calculer $x^*(t)$ en minimisant $tf_0 + \phi$ soumis à $Ax = b$. \\
		$x \gets x^*(t)$
	}
\end{algorithm}

A la fin de cet algorithme on a donc :
$$ f_0(x) - p^* \leqslant \epsilon $$
Le nombre d'itération de centrage est :
$$ n_{step} = \left\lceil \dfrac{\log \left( m / \left( \epsilon t^{(0)} \right) \right)}{\log \mu} \right\rceil $$